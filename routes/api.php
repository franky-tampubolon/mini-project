<?php

use Illuminate\Http\Request;
// use Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('/register', function(){
//     return view('auth.register');
// });
// Route::get('/login', function(){
//     return view('auth.login');
// });


// Route::post('register/proses', 'RegisterController@proses');
// Route::post('login/proses', 'LoginController@proses');

// Route::middleware('auth:api')->group(function(){
//     Route::post('/logout', 'LoginController@logout');
// });
// Route::get('/home', 'HomeController@index')->name('home');


// Route::get('/login/{provider}', 'LoginController@redirectToProvider');
// Route::get('/login/{provider}/callback', 'LoginController@handleProviderCallback');


Route::group([
    'middleware'    => 'api',
    'prefix'        => 'auth',
    'namespace'     => 'Auth'
], function($router){
    Route::post('/register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::get('social/{provider}', 'SocialiteController@redirectToProvider');
    Route::get('social/{provider}/callback', 'SocialiteController@handleProviderCallback');
});


Route::group([
    'middleware'       => 'api'
], function($route){
    Route::post('role', 'RoleController@store');
    Route::post('route', 'RouteController@store');
    Route::post('event', 'EventController@store');
    Route::post('perusahaan', 'PerusahaanController@store');
});
Route::post('daftar-event-perusahaan', 'DaftarEventController@perusahaan');
Route::post('daftar-event-peserta', 'DaftarEventController@peserta');