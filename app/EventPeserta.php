<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPeserta extends Model
{
    protected $guarded = ['id'];
    protected $table = 'event_peserta';
}
