<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    //
    protected $guarded = ['id'];

    protected $table ='social_media';


    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
