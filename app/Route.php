<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $guarded = ['id'];

    protected $table = 'routes';

    public function user()
    {
        return $this->belongsToMany('App\User', 'App\RouteUser', 'route_id', 'user_id');
    }
}
