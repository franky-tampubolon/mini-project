<?php

namespace App;

use App\User;
use App\RoleUser;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guarded = ['id'];
    protected $table = 'roles';


    public function user()
    {
        return $this->belongsToMany('App\User', 'App\RoleUser', 'role_id', 'user_id');
    }
    
}
