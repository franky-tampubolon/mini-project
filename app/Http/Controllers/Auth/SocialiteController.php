<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Middleware\SocialMedia as MiddlewareSocialMedia;
use App\SocialMedia;
use App\User;
use Illuminate\Http\Request;
use Socialite;
use JWTAuth;

class SocialiteController extends Controller
{
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->stateless()->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        $data = [];
        $socialUser = Socialite::driver($provider)->stateless()->user();

        $socialAccount = SocialMedia::where('provider_id', $socialUser['id'])->where('provider_name', $provider)->first();
        if($socialAccount){
            $user = $socialAccount->user;
        }else{
            $user = User::where('email', $socialUser->getEmail())->first();
            if(! $user){
                $user = User::firstOrCreate([
                    'name'  => $socialUser['name'],
                    'email' => $socialUser['email']
                ]);
            }
        }

        SocialMedia::firstOrCreate([
            'provider_id'   => $socialUser['id'],
            'provider_name' => $provider,
            'user_id'       => $user->id
        ]);
        
        $data['token'] = JWTAuth::fromUser($user);
        $data['user'] = $user;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'user berhasil login',
            'data'      => $data
        ], 200);
    }
}
