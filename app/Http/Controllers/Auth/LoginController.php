<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Auth;
class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(LoginRequest $request)
    {
        $data = [];
        $credential = $request->only(['no_hp', 'password']);
        // jika gagal
        if(! $token = auth()->attempt($credential)){
            return response()->json([
                'response_message'  => 'Unauthorized'
            ], 401);
        }

        // jika berhasil
        $data['token'] = $token;
        $data['user'] = Auth::user();

        return response()->json([
            'response_code' => '00',
            'response_message'  => 'user berhasil login',
            'data'      => $data
        ], 200);
    }
}
