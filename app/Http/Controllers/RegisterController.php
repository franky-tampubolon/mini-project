<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class RegisterController extends Controller
{
    public function proses(Request $request)
    {
        $data = [
            'name'  => $request->name,
            'email'  => $request->email,
            'no_hp'  => $request->no_hp,
            'password'  => bcrypt($request->password),
        ];


        User::create($data);
        $credentials = $request->only('no_hp', 'password');
        $token = auth()->attempt($credentials);

        return redirect()->route('home', $token);
    }
}
