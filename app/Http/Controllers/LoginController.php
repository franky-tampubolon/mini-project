<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    public function proses(Request $request)
    {
        $credentials = $request->only('no_hp', 'password');
        // dd($credentials);
        if(! $token = auth()->attempt($credentials)){
            dd($token);
            return response()->json(['error' => 'invalid_credentials'], 401);
        }

        return redirect()->route('home');
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        // $user->token;
    }
}
