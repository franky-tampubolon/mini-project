<?php

namespace App\Http\Controllers;

use App\Perusahaan;
use App\Http\Requests\PerusahaanRequest;
use Illuminate\Support\Facades\Auth;

class PerusahaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PerusahaanRequest $request)
    {
        $perusahaan = Perusahaan::create([
            'nama_perusahaan'   => $request->nama_perusahaan,
            'alamat'   => $request->alamat,
            'penanggung_jawab'   => Auth::user()->id,
        ]);

        $newToken = auth()->refresh();
        $data['token'] = $newToken;
        $data['perusahaan'] = $perusahaan;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Perusahaan berhasil ditambahkan',
            'data'      => $data
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PerusahaanRequest $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
